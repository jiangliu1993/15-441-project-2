#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include <netinet/in.h>
#include "chunk.h"
#include "connection.h"
#include "debug.h"

/******************************************************************************/
/******************************************************************************/
/***************************** Helper Functions *******************************/

long long now()
{
    struct timeval now;
    long long microsecond;

    gettimeofday(&now, NULL);
    microsecond = now.tv_sec;
    microsecond = microsecond * 1000000 + now.tv_usec;

    return microsecond;
}

static int min(int x, int y)
{
    if (x < y)
        return x;
    return y;
}

/******************************************************************************/
/******************************************************************************/
/******************************** Buffer **************************************/
buffer_t* connection_buffer_create()
{
    buffer_t* instance = malloc(sizeof(buffer_t));

    instance->init = connection_buffer_init;
    instance->put = connection_buffer_put;
    instance->finalize = connection_buffer_finalize;

    return instance;
}

void connection_buffer_init(buffer_t* self, int type, int wnd_size)
{
    assert(wnd_size < CONNECTION_MAX_BUF_SIZE);
    int i;

    for (i = 0; i < CONNECTION_MAX_BUF_SIZE; ++i)
        self->buffer[i] = NULL;
    self->type = type;
    self->last_sent = self->last_read = 0;
    self->last_acked = self->cnt_packets = 0;
    self->wnd_size = wnd_size;
    self->avoid_count = 0;
    self->ssthresh = INITIAL_SSTHRESH;
    self->dup_acks = 0;
}

int connection_buffer_put(buffer_t* self, packet* p)
{
    assert(p != NULL);
    assert(self->cnt_packets < CONNECTION_MAX_BUF_SIZE);
    int num;

    num = p->header->sequence;

    // Already have the packet?
    if (self->buffer[POS(num)] != NULL)
        return -1;

    self->buffer[POS(num)] = p;
    self->cnt_packets += 1;

    return 0;
}

void connection_buffer_finalize(buffer_t* self)
{
    int i;

    for (i = 0; i < CONNECTION_MAX_BUF_SIZE; ++i)
        if (self->buffer[i] != NULL)
            free_packet(self->buffer[i]);

    free(self);
}

/******************************************************************************/
/******************************************************************************/
/**************************** Connection **************************************/
connection_t* connection_create()
{
    connection_t* instance = malloc(sizeof(connection_t));

    instance->init = connection_init;
    instance->finalize = connection_finalize;
    instance->send = connection_send;
    instance->recv_ack = connection_recv_ack;
    instance->recv_data = connection_recv_data;
    instance->read = connection_read;
    instance->handle_timeout = connection_handle_timeout;
    instance->send_GET = connection_send_GET;

    return instance;
}

void connection_init(connection_t* self, int id, int type, int sock,
                     sockaddr_in_t* addr)
{
    self->id = id;
    self->type = type;
    self->seq = 1;
    self->last_ack = 0;
    self->buf = connection_buffer_create();
    if (type == C_SENDER) {
        self->buf->init(self->buf, type, 1);
    }
    else {
        self->buf->init(self->buf, type, CONNECTION_MAX_BUF_SIZE - 1);
    }
    // Write initial window size to file
    update_wnd_size(self->id, self->buf->wnd_size);
    self->sock = sock;
    self->addr = addr;
    self->p_timeout = PACKET_DEFAULT_TIMEOUT;
}

void connection_set_packet_timeout(connection_t* self, int timeout)
{
    self->p_timeout = timeout;
}

void connection_finalize(connection_t* self)
{
    connection_buffer_finalize(self->buf);
    free(self);
}

/**
 * Send any unsent packets in sender buffer. Timestamp of packets will be
 * noted down.
 */
static void _send_packets(connection_t* self)
{
    buffer_t* buf = self->buf;
    packet* p;

    while ((p = buf->buffer[NEXT(buf->last_sent)]) != NULL &&
           (int)SENDING(buf->last_acked, buf->last_sent) < buf->wnd_size) {
        send_packet_to(self->sock, self->addr, p);
        buf->last_sent = NEXT(buf->last_sent);
        buf->timestamp[buf->last_sent] = now();
    }
}

int connection_send(connection_t* self, char* buf, int n)
{
    int bytes_sent = 0;
    int payload_len;
    packet* p;

    while (self->buf->cnt_packets < CONNECTION_MAX_BUF_SIZE &&
           bytes_sent < n) {
        //while (self->buf->cnt_packets < self->buf->wnd_size && bytes_sent < n) {
        // Marshal data into packets
        payload_len = min(n - bytes_sent, MAX_PAYLOAD_SIZE);
        p = create_packet(T_DATA, self->seq++, 0, buf + bytes_sent,
                          payload_len);
        if (p == NULL) {
            self->seq--;
            perror("connection_send: failed to create packet");
            break;
        }
        if (self->buf->put(self->buf, p) == -1) {
            free_packet(p);
            self->seq--;
            break;
        }
        bytes_sent += payload_len;
    }

    _send_packets(self);

    return bytes_sent;
}

int connection_recv_ack(connection_t* self, packet* ack)
{
    buffer_t* buf = self->buf;
    packet* p;
    int cnt_ack = 0;
    int valid = 0;

    while ((p = buf->buffer[NEXT(buf->last_acked)]) != NULL) {
        if (p->header->sequence > ack->header->acknowledge) {
            if (p->header->sequence - 1 == ack->header->acknowledge) {
                /* Duplicated ack */
                buf->dup_acks++;
                if (buf->dup_acks == 3) {
                    /* Fast retransmit */
                    buf->last_sent = buf->last_acked;
                    buf->ssthresh = buf->wnd_size/2 > 2 ? buf->wnd_size/2 : 2;
                    buf->wnd_size = 1;
                    buf->avoid_count = 0;
                    update_wnd_size(self->id, buf->wnd_size);
                }
            }
            else {
                valid = 1;
            }
            break;
        }
        valid = 1;
        buf->dup_acks = 0;
        if (buf->last_acked == buf->last_sent) {
            buf->last_sent = NEXT(buf->last_sent);
        }
        buf->last_acked = NEXT(buf->last_acked);
        cnt_ack += p->payload->data_len;
        buf->buffer[buf->last_acked] = NULL;
        free_packet(p);
        buf->cnt_packets--;
    }
    if (valid && buf->wnd_size + 1 < CONNECTION_MAX_BUF_SIZE) {
        if (buf->wnd_size < buf->ssthresh) {
            buf->wnd_size++;
            update_wnd_size(self->id, buf->wnd_size);
        }
        else {
            buf->avoid_count++;
            if (buf->avoid_count >= buf->wnd_size) {
                buf->wnd_size++;
                update_wnd_size(self->id, buf->wnd_size);
                buf->avoid_count = 0;
            }
        }
    }

    return cnt_ack;
}

void connection_recv_data(connection_t* self, packet* data)
{
    buffer_t* buf = self->buf;
    unsigned int next;
    packet* p;

    // Drop packets that have already been acked
    if (data->header->sequence <= self->last_ack) return;
    // Drop packets if it's out of current recervier window
    if (data->header->sequence > self->last_ack + self->buf->wnd_size) return;

    p = clone_packet(data);
    if (p == NULL)
        fprintf(stderr, "connection_recv_data: failed to clone packet\n");

    // Put packet into receiver buffer
    if (buf->put(buf, p) == -1)
        return;

    // Update cumulative ACK number
    while (1) {
        next = NEXT(buf->last_acked);
        if (next == buf->last_read || buf->buffer[next] == NULL)
            break;
        self->last_ack = buf->buffer[next]->header->sequence;
        buf->last_acked = NEXT(buf->last_acked);
    }

    // Response with ACK
    p = create_packet(T_ACK, 0, self->last_ack, NULL, 0);
    send_packet_to(self->sock, self->addr, p);
    free_packet(p);
}

int connection_read(connection_t* self, char* buf, int max)
{
    buffer_t* recv_buf = self->buf;
    int bytes_read = 0;
    packet* p;

    while (recv_buf->last_read != recv_buf->last_acked) {
        // The first packet in receiver window
        p = recv_buf->buffer[NEXT(recv_buf->last_read)];
        // The buffer is not big enough to hold packet payload
        if (bytes_read + p->payload->data_len > max)
            break;
        // Copy packet payload into buffer
        memcpy(buf + bytes_read, p->payload->data, p->payload->data_len);
        bytes_read += p->payload->data_len;

        recv_buf->last_read = NEXT(recv_buf->last_read);
        // Remove the packet from receiver buffer
        free_packet(p);
        recv_buf->buffer[recv_buf->last_read] = NULL;
        --recv_buf->cnt_packets;
    }

    return bytes_read;
}

int connection_handle_timeout(connection_t* self)
{
    // This method only makes sense when called by a sender
    assert(self->type == C_SENDER);

    int cnt_timeout = 0;
    long long microsecond = now();

    buffer_t* buf = self->buf;
    unsigned int i = NEXT(buf->last_acked);
    int flag = 1;

    for (; flag; i = NEXT(i)) {
        flag = (i != buf->last_sent);
        if (buf->buffer[i] != NULL &&
                microsecond - buf->timestamp[i] > self->p_timeout) {
            // The packet has timeout
            buf->last_sent = buf->last_acked;
            cnt_timeout += 1;
            buf->ssthresh = buf->wnd_size/2 > 2 ? buf->wnd_size/2 : 2;
            buf->wnd_size = 1;
            buf->avoid_count = 0;
            update_wnd_size(self->id, buf->wnd_size);
            _send_packets(self);
            break;
        }
    }

    return cnt_timeout;
}

void connection_send_GET(connection_t* self, char* hash)
{
    packet* p = create_packet(T_GET, 0, 0, hash, CHUNK_HASH_LEN);
    send_packet_to(self->sock, self->addr, p);
    free_packet(p);
}
