/**
 * Peer packets definition and operations
 */

#ifndef PACKET_H
#define PACKET_H

#include "constants.h"

/**
 * Packet constants
 */
#define MAGIC_NUM 15441
#define VERSION_NUM 1
#define GROUP_NUM 6415

/**
 * Packet types
 */
#define T_WHOHAS 0
#define T_IHAVE 1
#define T_GET 2
#define T_DATA 3
#define T_ACK 4
#define T_DENIED 5
#define MAX_TYPE 5


/**
 *
 */
typedef struct sockaddr_in sockaddr_in_t;

/**
 * Packet structure
 */

typedef struct packet_header {
    unsigned short magic;
    unsigned char version;
    unsigned char type;
    unsigned short header_len;
    unsigned short total_len;
    unsigned int sequence;
    unsigned int acknowledge;
} packet_header;

typedef struct packet_extend_header {
    unsigned short group;
} packet_extend_header;

typedef struct packet_payload {
    char *data;
    unsigned short data_len;
} packet_payload;

typedef struct packet {
    packet_header *header;
    packet_extend_header *extend_header;
    packet_payload *payload;
} packet;

/**
 * Create a normal packet, return NULL on error
 */
packet *create_packet(unsigned char type, unsigned int sequence,
                      unsigned int acknowledge, char *data,
                      unsigned short data_len);

/**
 * Clone a packet return NULL on error
 */
packet* clone_packet(packet* p);

/**
 * Create an extended packet, return NULL on error
 */
packet *create_extend_packet(unsigned char type, unsigned int sequence,
                             unsigned int acknowledge, char *data,
                             unsigned short data_len);

/**
 * Send a packet within a UDP datagram to addr Return 1 on success, 0 on error
 */
int send_packet_to(int sock, sockaddr_in_t *addr, packet *p);

/**
 * Convert a packet structure to UDP datagram bytes.
 * Return bytes length on success, 0 on error.
 */
int packet_to_bytes(packet *p, char *buf, int buf_len);

/**
 * Parse a packet structure from UDP datagram bytes.
 * Return NULL if the datagram is malformed.
 */
packet *bytes_to_packet(char *buf, int buf_len);

/**
 * Free a packet
 */
void free_packet(packet *p);

/**
 * Create a normal packet header, return NULL on error
 */
packet_header *create_header(unsigned char type, unsigned int sequence,
                             unsigned int acknowledge, unsigned short header_len,
                             unsigned short total_len);

/**
 * Convert a packet header structure to bytes.
 * Return bytes length on success, 0 on error.
 */
int header_to_bytes(packet_header *header, char *buf, int buf_len);

/**
 * Parse a packet header structure from bytes.
 * Return NULL is the bytes are malformed.
 */
packet_header *bytes_to_header(char *buf, int buf_len);

/**
 * Free a normal header
 */
void free_header(packet_header *header);

/**
 * Create an extend header, return NULL on error
 */
packet_extend_header *create_extend_header();

/**
 * Convert a packet extend header structure to bytes.
 * Return bytes length on success, 0 on error.
 */
int extend_header_to_bytes(packet_extend_header *extend_header,
                           char *buf, int buf_len);

/**
 * Parse a packet extend header structure from bytes.
 * Return NULL is the bytes are malformed.
 */
packet_extend_header *bytes_to_extend_header(char *buf, int buf_len);

/**
 * Free an extend header
 */
void free_extend_header(packet_extend_header *extend_header);

/**
 * Create the packet payload, return NULL on error
 */
packet_payload *create_payload(char *data, unsigned short data_len);

/**
 * Convert a packet payload structure to bytes.
 * Return bytes length on success, 0 on error.
 */
int payload_to_bytes(packet_payload *payload, char *buf, int buf_len);

/**
 * Parse a packet payload structure from bytes.
 * Return NULL is the bytes are malformed.
 */
packet_payload *bytes_to_payload(char *buf, int buf_len);

/**
 * Free the packet payload
 */
void free_payload(packet_payload *payload);

/**
 * Convert an unsigned int to 4 bytes in network order
 */
void uint_to_bytes(unsigned int value, char *buf);

/**
 * Convert an unsigned short to 2 bytes in network order
 */
void ushort_to_bytes(unsigned short value, char *buf);

/**
 * Convert an unsigned char to 1 bytes in network order
 */
void uchar_to_bytes(unsigned char value, char *buf);

/**
 * Convert 4 bytes in network order to an unsigned int.
 */
unsigned int bytes_to_uint(char *buf);

/**
 * Convert 2 bytes in network order to an unsigned short.
 */
unsigned short bytes_to_ushort(char *buf);

/**
 * Convert 1 bytes in network order to an unsigned char.
 */
unsigned char bytes_to_uchar(char *buf);

#endif
