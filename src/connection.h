#ifndef __CONNECTION_H__
#define __CONNECTION_H__

#include "constants.h"
#include "packet.h"

/**
 * Return microseconds since epoch
 */
long long now();
/******************************************************************************/
/******************************************************************************/
/******************************** Buffer **************************************/
/**
 * wnd_size *MUST* smaller than CONNECTION_MAX_BUF_SIZE
 */
#define CONNECTION_MAX_BUF_SIZE 4096
#define INITIAL_SSTHRESH 64
#define C_SENDER 0
#define C_RECEIVER 1
#define POS(i) (i % CONNECTION_MAX_BUF_SIZE)
#define PREV(i) ((i - 1 + CONNECTION_MAX_BUF_SIZE) % CONNECTION_MAX_BUF_SIZE)
#define NEXT(i) ((i + 1) % CONNECTION_MAX_BUF_SIZE)
#define SENDING(i, j) (i <= j ? j - i : j + CONNECTION_MAX_BUF_SIZE - i)

typedef struct buffer_s buffer_t;
struct buffer_s {
    packet* buffer[CONNECTION_MAX_BUF_SIZE];
    long long timestamp[CONNECTION_MAX_BUF_SIZE];
    int type; //<! The type of this buffer, sender buffer or receiver buffer
    unsigned int last_acked; //<! For both sender and receiver buffer
    unsigned int last_sent; //<! For sender buffer only
    unsigned int last_read; //<! For receiver buffer only
    int wnd_size;
    int avoid_count;
    int ssthresh;
    int cnt_packets;
    int dup_acks;

    /**
     * Initialize a buffer
     *
     * @param type The type of this buffer.
     * @param wnd_size The initial window size of the buffer
     */
    void (*init)(buffer_t* self, int type, int wnd_size);

    /**
     * Try put a packet into the sender buffer. The position of the packet in
     * sender buffer is determined by its sequence number. When putting a packet
     * to position that is not vacant, the new packet will be ignored and this
     * leads to a return value of -1
     *
     * @return If the number of packets in the buffer now is smaller than
     * wnd_size and the corresponding position for the packet is vacant, 0 will
     * be returned. Otherwise, return -1.
     */
    int (*put)(buffer_t* self, packet* p);

    /**
     * Finalize a buffer_t. Free all resources
     */
    void (*finalize)(buffer_t* self);
};

buffer_t* connection_buffer_create();

/**
 * Methods in buffer_t
 */
void connection_buffer_init(buffer_t*, int, int);
int connection_buffer_put(buffer_t*, packet*);
void connection_buffer_finalize(buffer_t*);

/******************************************************************************/
/******************************************************************************/
/**************************** Connection **************************************/

/**
 * A connection is a one way dataflow transmitting data from an address to
 * localhost or from localhost to an address.
 */
typedef struct connection_s connection_t;
struct connection_s {
    int id; //<! Connection id
    int sock;  //<! Connection socket
    int seq; //<! Current sequence number
    unsigned int
    last_ack; //<! The acknowledgement number sent last time(initially 0)
    int type; //<! The role of localhost in this connection
    sockaddr_in_t* addr; //<! Receiver address
    buffer_t* buf; //<! The sender/receiver buffer(stores packets)
    int p_timeout; //<! Timeout for a packet in microsecond(10^-6 s)

    /**
     * Initialize a connection. Packet timeout will be set to P_DEFAULT_TIMEOUT
     *
     * @param id Connection id
     * @param type In this connection, localhost will serve as sender or
     *             receiver? (C_SENDER or C_RECEIVER)
     * @param sock The socket where packets will be sent in this connection
     * @param addr The address of destination
     */
    void (*init)(connection_t* self, int id, int type, int sock,
                 sockaddr_in_t* addr);

    /**
     * Update packet timeout
     */
    void (*set_packet_timeout)(connection_t* self, int timeout);

    /**
     * Finalize a connection. Free all resources
     */
    void (*finalize)(connection_t* self);

    /**
     * Send n bytes through this connection
     *
     * @param buf The buffer holding bytes to be sent
     * @param n The number of bytes to be sent
     * @return Number of bytes sent.
     */
    int (*send)(connection_t* self, char* buf, int n);

    /**
     * Handle an ACK packet. Adjust internal sliding windows
     *
     * @return Number of bytes ACKed
     */
    int (*recv_ack)(connection_t* self, packet* p);

    /**
     * Handle a DATA packet. Send ACK back accordingly.
     */
    void (*recv_data)(connection_t* self, packet* p);

    /**
     * Read bytes from the connection. Payload of ACKed packets will be put
     * into the buffer provided. The method will NOT put part of data into the
     * provided buffer. In other word, if remaining space is not enough to
     * hold complete data in a packet, the method will return without filling
     * those remaining space. Once data in a packet has been read, the packet
     * will be delete.
     *
     * @param self A pointer to the connection
     * @param buf A buffer used to stored bytes
     * @param max Maximum number of bytes can be read into the buffer
     * @return Number of bytes read
     */
    int (*read)(connection_t* self, char* buf, int max);

    /**
     * Go through each packet in buffer and check if they've timeout
     *
     * @return Number of timeout packet
     */
    int (*handle_timeout)(connection_t* self);

    /**
     * Send GET message with given hash through the connection
     */
    void (*send_GET)(connection_t* self, char* hash);
};

connection_t* connection_create();

/**
 * Methods in connection_t
 */
void connection_init(connection_t*, int, int, int, sockaddr_in_t*);
void connection_set_packet_timeout(connection_t*, int);
void connection_finalize(connection_t*);
int connection_send(connection_t*, char*, int);
int connection_recv_ack(connection_t*, packet*);
void connection_recv_data(connection_t*, packet*);
int connection_read(connection_t*, char*, int);
int connection_handle_timeout(connection_t*);
void connection_send_GET(connection_t*, char*);

#endif
