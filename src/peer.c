#include <assert.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <sys/select.h>
#include <sys/types.h>
#include <string.h>
#include <unistd.h>
#include "peer.h"
#include "debug.h"
#include "spiffy.h"
#include "message.h"

peer_t* peer_create()
{
    peer_t* instance = malloc(sizeof(peer_t));

    instance->init = peer_init;
    instance->finalize = peer_finalize;
    instance->setup_socket = peer_setup_socket;
    instance->dump = peer_dump;
    instance->dispatch = peer_dispatch;
    instance->handle_whohas = peer_handle_whohas;
    instance->broadcast_whohas = peer_broadcast_whohas;
    instance->identify = peer_identify;
    instance->handle_timeout = peer_handle_timeout;
    instance->new_download = peer_new_download;

    return instance;
}

int peer_setup_socket(peer_t *self)
{
    assert(self != NULL);

    struct sockaddr_in myaddr;

    if ((self->sock = socket(AF_INET, SOCK_DGRAM, 0)) == -1) {
        perror("peer_setup_socket socket()");
        return -1;
    }

    memset((char *)&myaddr, 0, sizeof(myaddr));
    myaddr.sin_family = AF_INET;
    inet_aton("127.0.0.1", (struct in_addr *)&myaddr.sin_addr.s_addr);
    myaddr.sin_port = htons(self->port);

    if (bind(self->sock, (struct sockaddr *) &myaddr, sizeof(myaddr)) == -1) {
        perror("peer_setup_socket bind()");
        return -1;
    }

    if (spiffy_init(self->identity, (struct sockaddr *)&myaddr,
                    sizeof(myaddr)) == -1) {
        perror("peer_setup_socket spiffy_init()");
        return -1;
    }

    return 0;
}

static int peer_read_master(peer_t *self, char *fname)
{
    FILE *fp;
    char resource_name[PEER_BUFSIZE];

    if ((fp = fopen(fname, "r")) == NULL) {
        perror("peer_read_master fopen() open master_file error");
        return -1;
    }

    if (fscanf(fp, "File: %200s", resource_name) < 1) {
        perror("peer_read_master fscanf()");
        return -1;
    }

    if ((self->master_file = fopen(resource_name, "rb")) == NULL) {
        perror("peer_read_master fopen() open resource error");
        return -1;
    }

    return 0;
}

int peer_init(peer_t *self, bt_config_t *cfg)
{
    assert(self != NULL);
    assert(cfg != NULL);

    self->peers = cfg->peers;
    self->max_conn = cfg->max_conn;
    self->identity = cfg->identity;
    self->port = cfg->myport;

    if (peer_read_master(self, cfg->chunk_file) == -1) return -1;
    if ((self->chunks = chunk_list_from_file(cfg->has_chunk_file)) == NULL)
        return -1;

    if (self->setup_socket(self) == -1)
        return -1;

    self->upload_pool = upload_manager_create();
    self->upload_pool->init(self->upload_pool, self->sock, self->max_conn,
                            self->master_file, self->chunks);

    self->download_pool = NULL;

    return 0;
}

void peer_finalize(peer_t *self)
{
    free_chunk_list(self->chunks);
    fclose(self->master_file);
    close(self->sock);
    free(self);
}

static void dump_chunk_list(chunk_list *chunks)
{
    chunk *pc;
    int i;

    for (pc = chunks->head; pc != NULL; pc = pc->next) {
        printf("chunk %d\n", pc->id);
        for (i = 0; i < CHUNK_HASH_LEN; ++i)
            printf("%02x", pc->hash[i] & 0xff);
        printf("\n");
    }
}

void peer_dump(peer_t *self)
{
    dump_chunk_list(self->chunks);
}

int peer_dispatch(peer_t *self)
{
    sockaddr_in_t from;
    socklen_t fromlen;
    char buf[PEER_BUFSIZE];
    int buf_len;
    packet* p;
    chunk_list* list;
    bt_peer_t* from_peer;

    fromlen = sizeof(from);
    buf_len = spiffy_recvfrom(self->sock, buf, PEER_BUFSIZE, 0,
                              (struct sockaddr *) &from, &fromlen);
    if (buf_len <= 0) {
        DEBUG_PERROR("peer_dispatch: spiffy recvfrom error");
        return -1;
    }

    from_peer = self->identify(self, &from);

    if (from_peer == NULL) {
        DPRINTF(DEBUG_ERRS, "peer_dispatch: unexpected source address\n");
        return -1;
    }

    p = bytes_to_packet(buf, buf_len);
    if (p == NULL) {
        DPRINTF(DEBUG_ERRS, "peer_dispath: bad message\n");
        return -1;
    }
    switch (p->header->type) {
    case T_WHOHAS:
        list = parse_whohas(p);
        self->handle_whohas(self, list, &from_peer->addr);
        free_chunk_list(list);
        break;

    case T_IHAVE:
        list = parse_ihave(p);
        if (self->download_pool)
            self->download_pool->handle_IHAVE(self->download_pool,
                                              &from_peer->addr, list);
        free_chunk_list(list);
        break;

    case T_GET:
        if (self->upload_pool)
            if (self->upload_pool->handle_GET(self->upload_pool,
                                              &from_peer->addr, p) == -1) {
                /*
                 * If a connection cannot be set up for incoming GET, response
                 * T_DENIED
                 */
                free_packet(p);
                p = create_packet(T_DENIED, 0, 0, NULL, 0);
                send_packet_to(self->sock, &from, p);
            }
        break;

    case T_DATA:
        if (self->download_pool) {
            self->download_pool->handle_DATA(self->download_pool,
                                             &from_peer->addr, p);
            if (self->download_pool->chunks_left == 0) {
                printf("GOT %s\n", self->download_pool->get_chunk_file);
                self->download_pool->finalize(self->download_pool);
                self->download_pool = NULL;
            }
        }
        break;

    case T_ACK:
        if (self->upload_pool)
            self->upload_pool->handle_ACK(self->upload_pool, &from_peer->addr,
                                          p);
        break;

    case T_DENIED:
        if (self->download_pool)
            self->download_pool->handle_DENIED(self->download_pool,
                                               &from_peer->addr);
        break;
    }

    free_packet(p);

    self->handle_timeout(self);

    return 0;
}

int peer_handle_whohas(peer_t *self, chunk_list *chunks, sockaddr_in_t *from)
{
    assert(chunks != NULL);
    bt_peer_t *p;

    chunk *c, *tmp;
    chunk_list *ihave = create_chunk_list();

    for (c = chunks->head; c != NULL; c = c->next)  {
        tmp = chunk_list_retrieve(self->chunks, c->hash);
        if (tmp != NULL)
            chunk_list_add(ihave, chunk_clone(tmp));
    }

    p = self->identify(self, from);
    if (p != NULL)
        send_ihave_to(self->sock, (sockaddr_in_t*)&p->addr, ihave);
    else
        DPRINTF(DEBUG_ERRS, "peer_handle_whohas: unexpected source address\n");

    free_chunk_list(ihave);

    return 0;
}

int peer_broadcast_whohas(peer_t *self, chunk_list *list)
{
    bt_peer_t *p;
    chunk *c;
    chunk_list *msg = create_chunk_list();

    for (c = list->head; c != NULL; c = c->next) {
        chunk_list_add(msg, chunk_clone(c));
        // Reach message capacity or not more chunk
        if (msg->length == PEER_CHUNK_PER_MSG || c->next == NULL) {
            for (p = self->peers; p != NULL; p = p->next)
                if (p->id != self->identity)
                    send_whohas_to(self->sock, (sockaddr_in_t*)&p->addr, msg);

            free_chunk_list(msg);
            if (c->next != NULL)
                msg = create_chunk_list();
        }
    }

    return 0;
}

bt_peer_t* peer_identify(peer_t* self, sockaddr_in_t* addr)
{
    bt_peer_t *p;

    for (p = self->peers; p != NULL; p = p->next)
        if (addr->sin_port == p->addr.sin_port &&
                addr->sin_addr.s_addr == p->addr.sin_addr.s_addr)
            return p;

    return NULL;
}

void peer_handle_timeout(peer_t* self)
{
    int cnt_timeout;

    self->upload_pool->handle_timeout(self->upload_pool);

    if (self->download_pool != NULL) {
        cnt_timeout = self->download_pool->handle_timeout(self->download_pool);
        if (cnt_timeout > 0)
            self->broadcast_whohas(self, self->download_pool->list);
    }
}

void peer_new_download(peer_t* self, char *get_chunk_file, chunk_list* chunks,
                       FILE* outfile)
{
    assert(self->download_pool == NULL);
    self->download_pool = download_create();

    self->download_pool->init(self->download_pool, get_chunk_file, outfile,
                              chunks, self->sock, self->max_conn);
}
