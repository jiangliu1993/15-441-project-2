--------------------------------
15441 Computer Network Project 2
--------------------------------

packet.[c|h] - The underlying packet definition and operations.
chunk.[c|h] - The definition and operations of chunks, hash, etc.
message.[c|h] - The operations for the upper level messages.
peer.[c|h] - Implementation of peer server
upload.[c|h] - Implementation of upload dataflows.
download.[c|h] - Implementation of download dataflows
connection.[c|h] - Implementation of reliable transmission and congestion
                   control
source.[c|h] - Provide functionalities to manage potential download sources
main.c - Main for peer

# From starter code
input buffer.[c|h] - Handle user input.
debug(-text).[c|h] - Helpful utilities for debugging output.
bt_parse.[c|h] - Utilities for parsing commandline arguments.
spiffy.[c|h] - Test network framework.

-------------------------
Design Decisions & Issues
-------------------------

# Packet structure
Extend_header structure and operations in every packet for future
extension, even we don't use it now.

# Timeout
Timeout is detected by maintaining timestamps and occasionally timeout from
select().

# Connection
Implemented in connection.c

# Download Source
Implemented in source.c.
In a download job, potential download sources(peers) are maintained by source
manager. Source manager manage a queue of download sources. When an IHAVE
message is received, it is handed to the source manager and the source manager
creates a new download source and append it to the end of the queue or update
existing download source in the queue. A download source will be removed from
the queue whenever a timeout occurs when downloading from the source. Source
manager also provide functionality to allocate a download source to given chunk.
In the allocation process, the first idle download source in the queue will be
returned.

# Download
Implemented in download.c
A download job maintains status from each chunk, PENDING, DOWNLOADING or
FINISHED. The update of chunk status is driven by receiving messages or
timeout:
    1) When an IHAVE message is received, it will be handed to source manager
    and all PENDING chunks will be checked to see if they can start downloading.

    2) When a DATA message is received, corresponding chunk will read data into
    its buffer and the chunk status will change to FINISHED once all data has
    been downloaded.

    3) DENIED or timeout. Corresponding chunk will change its status from
    DOWLOADING back to PENDING. Source manager will be informed and
    corresponding download source will be removed.

A download job finishes once all chunk is in status FINISHED

# Upload
Implemented in upload.c
Upload is hanle by upload manager which maintain a pool of upload dataflows.
Like download, upload is also driven by messages and timeout:
    1) When receiving a GET message, a new upload dataflow will be allocated
    if there are still idle dataflows in pool. The dataflow will try to send
    data once it's openned.

    2) When receiving a ACK message, corresponding dataflow will update its
    sender window and continue to send data if possible.

    3) Timeout. An ACK message is not received within expected time. The
    dataflow will adjust its window size and/or retransmit according to
    congestion control strategy.
