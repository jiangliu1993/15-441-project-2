#include "chunk.h"
#include "message.h"
#include "packet.h"
#include "source.h"
#define TEST_MAX_BUF 1000

void test(context_t* ctx)
{
    sockaddr_in_t a1, a2;
    chunk_list* cl1 = chunk_list_from_file("tmp/A.chunks");
    chunk_list* cl2 = chunk_list_from_file("tmp/B.chunks");
    source_manager_t* manager = source_manager_create();

    memset(&a1, 1, sizeof(a1));
    memset(&a2, 2, sizeof(a2));
    manager->init(manager);
    manager->add(manager, &a1, cl1);
    manager->add(manager, &a2, cl2);
    manager->query(manager, cl2->tail->hash);
    manager->remove(manager, &a2);
}
