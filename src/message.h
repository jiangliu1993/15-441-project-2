/**
 * Operations of the upper level message communication.
 */

#ifndef MESSAGE_H
#define MESSAGE_H

#include "chunk.h"
#include "packet.h"

/**
 * Send WHOHAS message to the peer on ip:port.
 * Return 1 on success, 0 on error.
 */
int send_whohas_to(int sock, sockaddr_in_t *addr, chunk_list *list);

/**
 * Send IHAVE message to the peer on ip:port
 * Return 1 on success, 0 on error.
 */
int send_ihave_to(int sock, sockaddr_in_t *addr, chunk_list *list);

/**
 * Send a packet with payload formate of a chunk list
 */
int send_chunk_list_to(int sock, sockaddr_in_t *addr, chunk_list *list,
                       unsigned char type);

/**
 * Parse a WHOHAS packet to the chunk list
 */
chunk_list *parse_whohas(packet *p);

/**
 * Parse a IHAVE packet to the chunk list
 */
chunk_list *parse_ihave(packet *p);

/**
 * Parse a packet with payload format of chunk list to a chunk list
 */
chunk_list *parse_chunk_list(packet *p);

#endif
