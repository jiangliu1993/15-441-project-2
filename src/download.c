#include <assert.h>
#include <stdlib.h>
#include <string.h>
#include <netinet/in.h>
#include "connection.h"
#include "debug.h"
#include "download.h"

download_chunk_t* download_chunk_create()
{
    download_chunk_t* instance = malloc(sizeof(download_chunk_t));

    instance->status = D_PENDING;
    instance->conn = NULL;
    instance->buf = NULL;
    instance->source = NULL;
    instance->bytes_recved = 0;

    instance->finalize = download_chunk_finalize;

    return instance;
}

void download_chunk_finalize(download_chunk_t* self)
{
    if (self->buf != NULL) {
        free(self->buf);
        self->buf = NULL;
    }
    if (self->conn != NULL) {
        self->conn->finalize(self->conn);
        self->conn = NULL;
    }

    if (self->source != NULL) {
        self->source->busy = 0;
        self->source = NULL;
    }
}

download_t* download_create()
{
    download_t* instance = malloc(sizeof(download_t));

    instance->init = download_init;
    instance->finalize = download_finalize;
    instance->handle_DATA = download_handle_DATA;
    instance->handle_IHAVE = download_handle_IHAVE;
    instance->new_connection = download_new_connection;
    instance->get_chunk_by_conn_addr = download_get_chunk_by_conn_addr;
    instance->schedule = download_schedule;
    instance->handle_DENIED = download_handle_DENIED;
    instance->handle_timeout = download_handle_timeout;
    instance->list = NULL;

    return instance;
}

void download_init(download_t* self, char *get_chunk_file, FILE* outfile,
                   chunk_list* chunks, int sock, int max_conn)
{
    chunk* c;
    download_chunk_t* dc;
    int i;
    long long time = now();

    self->connection_id = 1;
    self->outfile = outfile;
    strcpy(self->get_chunk_file, get_chunk_file);
    self->sock = sock;

    // Store download chunks information
    self->chunks_left = self->cnt_chunks = chunks->length;
    self->chunks = malloc(self->cnt_chunks * sizeof(download_chunk_t*));
    self->list = chunks;
    for (i = 0, c = chunks->head; i < self->cnt_chunks; i += 1, c = c->next) {
        dc = download_chunk_create();
        dc->id = c->id;
        memcpy(dc->hash, c->hash, CHUNK_HASH_LEN);
        dc->timestamp = time;
        self->chunks[i] = dc;
    }

    self->cnt_conn = 0;
    self->max_conn = max_conn;

    self->sources = source_manager_create();
    self->sources->init(self->sources);
}

void download_finalize(download_t* self)
{
    int i = 0;
    download_chunk_t* dc;

    for (; i < self->cnt_chunks; ++i) {
        dc = self->chunks[i];
        dc->finalize(dc);
        free(dc);
    }
    free(self->chunks);
    if (self->list)
        free_chunk_list(self->list);

    self->sources->finalize(self->sources);

    free(self);
}

void download_handle_DATA(download_t* self, sockaddr_in_t* from, packet* p)
{
    download_chunk_t* dc;
    int n;

    dc = self->get_chunk_by_conn_addr(self, from);
    if (dc != NULL) {
        // Record timestamp
        dc->timestamp = now();

        dc->conn->recv_data(dc->conn, p);
        n = dc->conn->read(dc->conn, dc->buf + dc->bytes_recved, CHUNK_DATA_SIZE
                           - dc->bytes_recved);

        if (n == -1) {
            DPRINTF(DEBUG_ERRS, "download_handle_DATA: read connection error\n");
            dc->finalize(dc);
            self->cnt_conn--;
        }
        dc->bytes_recved += n;

        // Chunk transmission finished
        if (dc->bytes_recved == CHUNK_DATA_SIZE) {
            if (fseek(self->outfile, dc->id * CHUNK_DATA_SIZE, SEEK_SET) == -1) {
                DEBUG_PERROR("download_handle_DATA: fseek error");
                dc->status = D_PENDING;
            } else {
                if (fwrite(dc->buf, 1, CHUNK_DATA_SIZE, self->outfile) == (size_t)-1) {
                    DEBUG_PERROR("download_handle_DATA: fwrite error");
                    dc->status = D_PENDING;
                } else {
                    self->chunks_left--;
                    if (self->chunks_left == 0)
                        fclose(self->outfile);
                    dc->status = D_FINISHED;
                }
            }
            dc->finalize(dc);
            self->cnt_conn--;
        }
    } else {
        DPRINTF(DEBUG_ERRS, "download_hanlde_DATA: packet from unknown \
            source\n");
    }

    self->schedule(self);
}

void download_handle_IHAVE(download_t* self, sockaddr_in_t* from,
                           chunk_list* chunks)
{
    self->sources->add(self->sources, from, chunks);

    self->schedule(self);
}

connection_t* download_new_connection(download_t* self, sockaddr_in_t* addr)
{
    connection_t* c;

    assert(self->cnt_conn <= self->max_conn);
    if (self->cnt_conn == self->max_conn) return NULL;

    c = connection_create();
    c->init(c, self->connection_id++, C_RECEIVER, self->sock, addr);
    self->cnt_conn++;
    return c;
}

download_chunk_t* download_get_chunk_by_conn_addr(download_t* self,
        sockaddr_in_t* addr)
{
    connection_t* c;
    int i;

    for (i = 0; i < self->cnt_chunks; ++i) {
        c = self->chunks[i]->conn;
        if (c != NULL && c->addr->sin_port == addr->sin_port &&
                c->addr->sin_addr.s_addr == addr->sin_addr.s_addr)
            return self->chunks[i];
    }

    return NULL;
}

void download_schedule(download_t* self)
{
    int i;
    download_chunk_t* dc;
    source_t* s;

    for (i = 0; i < self->cnt_chunks; ++i) {
        dc = self->chunks[i];

        if (dc->status != D_PENDING)
            continue;

        s = self->sources->query(self->sources, dc->hash);
        if (s != NULL) {
            dc->source = s;
            s->busy = 1;
            dc->conn = self->new_connection(self, s->addr);
            dc->bytes_recved = 0;
            dc->status = D_DOWNLOADING;
            dc->timestamp = now();
            dc->buf = malloc(CHUNK_DATA_SIZE * sizeof(char));
            dc->conn->send_GET(dc->conn, dc->hash);
        }
    }
}

void download_handle_DENIED(download_t* self, sockaddr_in_t* from)
{
    download_chunk_t* dc = self->get_chunk_by_conn_addr(self, from);
    if (dc == NULL)  {
        DPRINTF(DEBUG_ERRS, "download_handle_DENIED: DENIED from unknown \
            source\n");
    } else {
        if (dc->status == D_DOWNLOADING) {
            dc->finalize(dc);
            dc->status = D_PENDING;
            --self->cnt_conn;
        }
        if (dc->status == D_PENDING) {
            DPRINTF(DEBUG_ERRS, "download_handle_DENIED: DENIED a pending \
                chunk\n");
        }
        if (dc->status == D_FINISHED) {
            DPRINTF(DEBUG_ERRS, "download_handle_DENIED: DENIED a finished \
                chunk\n");
        }
    }

    self->schedule(self);
}

int download_handle_timeout(download_t* self)
{
    int i;
    download_chunk_t* dc;
    long long usec = now();
    int cnt_timeout = 0;

    for (i = 0; i < self->cnt_chunks; ++i) {
        dc = self->chunks[i];
        if (usec - dc->timestamp > DOWNLOAD_CHUNK_TIMEOUT) {
            if (dc->status == D_DOWNLOADING) {
                DPRINTF(DEBUG_ERRS, "Chunk timeout\n");
                self->sources->remove(self->sources, dc->source->addr);
                dc->source = NULL;
                dc->finalize(dc);
                dc->status = D_PENDING;
                self->cnt_conn--;
                cnt_timeout++;
            }
        }
    }

    return cnt_timeout;
}
