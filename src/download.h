#ifndef __DOWNLOAD_H__
#define __DOWNLOAD_H__

#include <stdio.h>
#include "chunk.h"
#include "constants.h"
#include "connection.h"
#include "source.h"

#define D_PENDING       0       // To be downloaded
#define D_FINISHED      1       // Downloading
#define D_DOWNLOADING   2       // Finished

typedef struct download_chunk_s download_chunk_t;
struct download_chunk_s {
    int id;
    int status;
    int bytes_recved;
    connection_t* conn;
    char* buf;
    char hash[CHUNK_HASH_LEN];
    source_t* source;
    long long timestamp;

    void (*finalize)(download_chunk_t* self);
};

download_chunk_t* download_chunk_create();

void download_chunk_finalize(download_chunk_t*);

/******************************************************************************/
/******************************************************************************/
/******************************* Download *************************************/
typedef struct download_s download_t;

struct download_s {
    download_chunk_t** chunks;
    chunk_list *list;
    char get_chunk_file[256];
    int cnt_chunks;
    int chunks_left;

    source_manager_t* sources;

    int connection_id;
    int max_conn, cnt_conn;

    FILE* outfile;
    int sock;

    /**
     * Initialize a download
     */
    void (*init)(download_t* self, char *get_chunk_file, FILE* outfile,
                 chunk_list* chunks, int sock, int max_conn);

    /**
     * Finalize a download
     */
    void (*finalize)(download_t* self);

    /**
     * Handle DATA packet
     */
    void (*handle_DATA)(download_t* self, sockaddr_in_t* from, packet* p);

    /**
     * Handle IHAVE packet and change pending chunk to status D_READY according
     * to strategy. Current strategy is turning chunk from D_PENDING to D_READY
     * at once if applicable.
     */
    void (*handle_IHAVE)(download_t* self, sockaddr_in_t* from, chunk_list*
                         chunks);

    /**
     * Create a new connection to given address
     *
     * @return A new connection if the number of connection in the download has
     *         not exceed max_conn. Otherwise NULL
     */
    connection_t* (*new_connection)(download_t* self, sockaddr_in_t* addr);

    /**
     * Retrieve a downloading chunk whose source address is addr
     *
     * @return A downloading chunk whose soure address is addr. NULL if not
     *         found
     */
    download_chunk_t* (*get_chunk_by_conn_addr)(download_t* self,
            sockaddr_in_t* addr);

    /**
     * Schedule chunks in D_READY status to start download if no other chunks
     * are downloading from the same source.
     */
    void (*schedule)(download_t* self);

    /**
     * Handle a DENIED packet, turn corresponding packet from status
     * D_DOWNLOADING to D_READY
     */
    void (*handle_DENIED)(download_t* self, sockaddr_in_t* from);

    /**
     * Go through all packets and turn timeout packet from status D_DOWNLOADING
     * to D_READY
     */
    int (*handle_timeout)(download_t* self);

};

download_t* download_create();

/**
 * Methods in download_t
 */
void download_init(download_t*, char*, FILE*, chunk_list*, int, int);
void download_finalize(download_t*);
void download_handle_DATA(download_t*, sockaddr_in_t*, packet*);
void download_handle_IHAVE(download_t*, sockaddr_in_t*, chunk_list*);
connection_t* download_new_connection(download_t*, sockaddr_in_t*);
download_chunk_t* download_get_chunk_by_conn_addr(download_t*, sockaddr_in_t*);
void download_schedule(download_t*);
void download_handle_DENIED(download_t*, sockaddr_in_t*);
int download_handle_timeout(download_t*);

#endif
