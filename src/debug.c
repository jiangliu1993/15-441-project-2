/*
 * A debugging helper library.
 */

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <sys/time.h>
#include "debug.h"

unsigned int debug = 0;

long long start_time;
FILE *wnd_plot_fp = NULL;

struct debug_def {
    int debug_val;
    char *debug_def;
};

/*
 * This list is auto-generated and included at compile time.
 * To add things, edit debug.h
 */

static
struct debug_def debugs[] = {
#include "debug-text.h"
    { 0, NULL } /* End of list marker */
};

static long long milli_now()
{
    struct timeval now;
    long long microsecond;

    gettimeofday(&now, NULL);
    microsecond = now.tv_sec * 1000 + now.tv_usec / 1000;

    return microsecond;
}

int set_debug(char *arg)
{
    int i;
    if (!arg || arg[0] == '\0') {
        return -1;
    }

    if (arg[0] == '?' || !strcmp(arg, "list")) {
        fprintf(stderr,
                "Debug values and definitions\n"
                "----------------------------\n");
        for (i = 0;  debugs[i].debug_def != NULL; i++) {
            fprintf(stderr, "%5d  %s\n", debugs[i].debug_val,
                    debugs[i].debug_def);
        }
        return -1;
    }

    if (isdigit(arg[0])) {
        debug |= atoi(arg);
    }
    return 0;
}

int init_wnd_plot()
{
    wnd_plot_fp = fopen("problem2-peer.txt", "w");
    if (!wnd_plot_fp) {
        return -1;
    }
    start_time = milli_now();

    return 0;
}

void update_wnd_size(int conn_id, int wnd_size)
{
    if (!wnd_plot_fp) {
        return;
    }
    fprintf(wnd_plot_fp, "f%d\t%lld\t%d\n", conn_id, milli_now() - start_time,
            wnd_size);
    fflush(wnd_plot_fp);
}

#ifdef _TEST_DEBUG_
int main()
{
    if (set_debug("?") != -1) {
        fprintf(stderr, "set_debug(\"?\") returned wrong result code\n");
    }
    exit(0);
}

#endif

/* Packet debugging utilities */

