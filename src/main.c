#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include <sys/select.h>
#include "debug.h"
#include "constants.h"
#include "bt_parse.h"
#include "input_buffer.h"
#include "peer.h"

#define MAX_BUF 256

typedef struct context_s {
    bt_config_t config;
    peer_t *server;
    struct user_iobuf *userbuf;
    struct timeval timeout;
} context_t;

#if TESTING

#include "test.h"

#endif

int run = 1;

void sig_handler(int sig)
{
    run = 0;
}

void init(context_t* ctx, int argc, char** argv);
void run_cmd(context_t* ctx);

int main(int argc, char **argv)
{
    context_t ctx;

    signal(SIGTERM, sig_handler);
    signal(SIGKILL, sig_handler);
    signal(SIGHUP, sig_handler);

    init(&ctx, argc, argv);

#if TESTING

    test(&ctx);

#endif
    run_cmd(&ctx);

    printf("Gracefully shut down\n");
    ctx.server->finalize(ctx.server);
    // Control should never reach here!
    //assert(0);

    return 0;
}

void init(context_t *ctx, int argc, char **argv)
{
    if ((ctx->userbuf = create_userbuf()) == NULL) {
        perror("peer_run could not allocate userbuf");
        exit(-1);
    }

    bt_init(&ctx->config, argc, argv);
    bt_parse_command_line(&ctx->config);
    if (init_wnd_plot() == -1) {
        perror("can't initialize window size plot output");
        exit(EXIT_FAILURE);
    }

    ctx->server = peer_create();
    if (ctx->server->init(ctx->server, &ctx->config) == -1) {
        exit(-1);
    }

    ctx->timeout.tv_sec = SELECT_TIMEOUT / 1000000;
    ctx->timeout.tv_usec = SELECT_TIMEOUT % 1000000;
}

void handle_user_input(char* line, void* cbdata)
{
    FILE* fp;
    context_t* ctx = (context_t*)cbdata;
    char cmd[MAX_BUF], chunkf[MAX_BUF], outputf[MAX_BUF];
    chunk_list *list;

    sscanf(line, "%s", cmd);

    while (1) {
        if (strcmp(cmd, "GET") == 0) {
            if (sscanf(line, "GET %200s %200s", chunkf, outputf) != 2) break;

            list = chunk_list_from_file(chunkf);
            if (list == NULL) break;

            if ((fp = fopen(outputf, "w")) == NULL) {
                perror("Cannot open output file for writing");
                break;
            }

            if (ctx->server->download_pool != NULL) {
                puts("Downloading! Can not create new download.");
                free_chunk_list(list);
                break;
            }

            if (ctx->server->broadcast_whohas(ctx->server, list) == -1)
                puts("GET error");
            else
                ctx->server->new_download(ctx->server, chunkf, list, fp);

            //free_chunk_list(list);
            break;
        }
        if (strlen(cmd) > 0) {
            puts("No such cmd");
        }
        break;
    }

    printf(">");
    fflush(stdout);
}

void run_cmd(context_t* ctx)
{
    fd_set readfds;
    int server_sock = ctx->server->sock;
    int ret;

    printf(">");
    fflush(stdout);
    while (run) {
        FD_ZERO(&readfds);
        FD_SET(STDIN_FILENO, &readfds);
        FD_SET(server_sock, &readfds);

        // ctx->timeout will change after select, need to reset.
        ctx->timeout.tv_sec = SELECT_TIMEOUT / 1000000;
        ctx->timeout.tv_usec = SELECT_TIMEOUT % 1000000;

        ret = select(server_sock + 1, &readfds, NULL, NULL, &ctx->timeout);

        if (ret < 0) {
            perror("run_cmd select()");
            return;
        }

        if (ret == 0)     // Timeout!
            ctx->server->handle_timeout(ctx->server);

        if (FD_ISSET(server_sock, &readfds)) {
            // Dispatch packet received and handle potential timeout
            ctx->server->dispatch(ctx->server);
        }

        if (FD_ISSET(STDIN_FILENO, &readfds)) {
            if (process_user_input(STDIN_FILENO, ctx->userbuf,
                                   handle_user_input, ctx) == -1)
                return;
        }
    }

}
