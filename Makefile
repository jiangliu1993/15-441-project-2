CC = gcc
CFLAGS = -g -Wall  # -DDEBUG -DTESTING=1

all: peer

peer: $(patsubst %.c, %.o, $(wildcard src/*.c))
	$(CC) $(CFLAGS) $^ -o peer

clean:
	rm -vf peer
	cd src; make clean

tar:
	(make clean; cd ..; tar cvf 15-441-project-2.tar 15-441-project-2)

submit:
	-git tag -d checkpoint-2
	git tag checkpoint-2
	make tar
