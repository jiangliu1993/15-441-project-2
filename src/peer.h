#ifndef __PEER_H__
#define __PEER_H__

#include <stdio.h>
#include "chunk.h"
#include "packet.h"
#include "bt_parse.h"
#include "connection.h"
#include "upload.h"
#include "download.h"

#define PEER_BUFSIZE 2048

typedef struct peer_s peer_t;
struct peer_s {
    FILE *master_file;
    chunk_list *chunks;
    bt_peer_t *peers;
    int max_conn;
    int identity;
    unsigned short port;
    int sock;
    upload_manager_t* upload_pool;
    download_t* download_pool;

    /**
     * Init a peer. Load configuration from cfg. Setup socket.
     * @param  self
     * @param  cfg
     * @return      0 on success, -1 on error
     */
    int (*init)(peer_t *self, bt_config_t *cfg);

    /**
     * Finalize peer. Free allocated memory. Close fds
     */
    void (*finalize)(peer_t *self);

    /**
     * setup socket
     * @param  self
     * @return      0 on success, -1 on error
     */
    int (*setup_socket)(peer_t *self);

    /**
     * print out internal structure of a peer.(for debug)
     */
    void (*dump)(peer_t *self);

    /**
     * dispatch inbound udp message to corresponding handler
     */
    int (*dispatch)(peer_t *self);

    /**
     * Handler for WHOHAS message and response with an IHAVE message back to
     * the source address.
     *
     * @param self      Invoker of this method
     * @param chunks    A list of chunks specified in the WHOHAS message
     * @param from      The source address of the WHOHAS message
     * @return          0 on success, -1 on error
     */
    int (*handle_whohas)(peer_t *self, chunk_list *chunks, sockaddr_in_t *from);

    /**
     * Broadcast WHOHAS message containing a list of chunks
     *
     * @param self      Invoker of this method
     * @param chunks    A list of chunks to be sent in the WHOHAS message
     * @return          0 on success, -1 on error
     */
    int (*broadcast_whohas)(peer_t *self, chunk_list *chunks);

    /**
     * Get bt_peer_t by source address
     *
     * @param self      Invoker of this method
     * @param addr      source address
     * @return          Pointer to bt_peer_t if found. NULL if not found
     */
    bt_peer_t* (*identify)(peer_t *self, sockaddr_in_t *addr);

    /**
     * Handle timeout.
     */
    void (*handle_timeout)(peer_t *self);

    /**
     * Create a new download to download given chunks to outfile
     */
    void (*new_download)(peer_t *self, char *get_chunk_file,
                         chunk_list* chunks, FILE* outfile);
};

/**
 * Create a peer server
 *
 * @return Pointer to a new peer server
 */
peer_t* peer_create();

/**
 * Methods in peer_s. peer->method = peer_method
 */
int peer_init(peer_t*, bt_config_t*);
void peer_finalize(peer_t*);
int peer_setup_socket(peer_t*);
void peer_dump(peer_t*);
int peer_dispatch(peer_t*);
int peer_handle_whohas(peer_t*, chunk_list*, sockaddr_in_t*);
int peer_broadcast_whohas(peer_t*, chunk_list*);
bt_peer_t* peer_identify(peer_t*, sockaddr_in_t*);
void peer_handle_timeout(peer_t*);
void peer_new_download(peer_t*, char*, chunk_list*, FILE*);

#endif
