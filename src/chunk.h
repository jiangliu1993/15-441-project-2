/**
 * Chunk and hash definition and operations
 */

#ifndef CHUNK_H
#define CHUNK_H

#include <stdio.h>
#include "constants.h"

#define CHUNK_BUFSIZE 50

/**
 * Chunk structure
 */

typedef struct chunk {
    int id;
    char hash[CHUNK_HASH_LEN];
    struct chunk *next;
} chunk;

typedef struct chunk_list {
    int length;
    chunk *head;
    chunk *tail;
} chunk_list;

/**
 * Create a chunk
 */
chunk *create_chunk(int id, char *hash);

/**
 * Clone a chunk
 */
chunk *chunk_clone(chunk *c);

/**
 * Parse a chunk from the packet payload.
 * Return NULL is the payload is malformed.
 */
chunk *bytes_to_chunk(char *buf, int buf_len);

/**
 * Free a chunk
 */
void free_chunk(chunk *c);

/**
 * Create an empty chunk list
 */
chunk_list *create_chunk_list();

/**
 * Clone a chunk list
 */
chunk_list *clone_chunk_list(chunk_list *list);

/**
 * Parse a chunk list from the packet payload.
 * Return NULL is the payload is malformed.
 */
chunk_list *bytes_to_chunk_list(char *buf, int buf_len);

/**
 * Add a chunk to the chunk list
 */
void chunk_list_add(chunk_list *list, chunk *c);

/**
 * Delete the chunk with the given id from the chunk list
 */
void chunk_list_remove(chunk_list *list, int id);

/**
 * Free a chunk list
 */
void free_chunk_list(chunk_list *list);

/**
 * Read chunk lists from a FILE pointer
 */
chunk_list *chunk_list_from_fp(FILE *fp);

/**
 * Read chunk lists from a file
 */
chunk_list *chunk_list_from_file(char *fname);

/**
 * Retrieve a chunk from a chunklist by hash. Return NULL if not found
 */
chunk *chunk_list_retrieve(chunk_list *list, char *hash);

/**
 * Check if two hash equal to each other
 *
 * @Return 1 if equal, 0 if not equal
 */
int hash_equal(char* h1, char* h2);

#endif
