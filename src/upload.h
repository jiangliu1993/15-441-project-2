#ifndef __UPLOAD_H__
#define __UPLOAD_H__

#include "chunk.h"
#include "connection.h"

/**
 * upload_t is a single upload dataflow
 */
typedef struct upload_s upload_t;
struct upload_s {
    connection_t* conn;
    char buffer[CHUNK_DATA_SIZE];
    int bytes_sent;
    int bytes_acked;
    int buf_len;
    long long timestamp;
    chunk_list* queue;

    /**
     * Initialize a upload.
     *
     * @param conn The connection for this upload
     */
    void (*init)(upload_t* self, connection_t* conn);

    /**
     * Finalize a upload
     */
    void (*finalize)(upload_t* self);

    /**
     * Handle an ACK packet
     */
    void (*handle_ACK)(upload_t* self, packet* p);

    /**
     * Handle timeout
     */
    int (*handle_timeout)(upload_t* self);

    /**
     * Send data in buffer through connection
     */
    void (*send_data)(upload_t* self);
};

upload_t* upload_create();

/**
 * Methods in upload_t
 */
void upload_init(upload_t*, connection_t*);
void upload_finalize(upload_t*);
void upload_handle_ACK(upload_t*, packet*);
int upload_handle_timeout(upload_t*);
void upload_send_data(upload_t*);


/******************************************************************************/
/******************************************************************************/
/****************************** Upload Manager ********************************/
/**
 * upload_manager_t manages all upload dataflows
 */
typedef struct upload_manager_s upload_manager_t;
struct upload_manager_s {
    chunk_list* local_chunks;
    FILE* master_file;
    int max; //<! The maximum number of concurrent upload dataflows
    int cnt; //<! The current number of concurrent upload dataflows
    int sock;
    int upload_id; //<! The connection id assigned to the next upload

    upload_t** upload_list;

    /**
     * Initialize a upload_manager_t
     *
     * @param sock A UDP socket to send data
     * @param max The maximum number of concurrent upload dataflows
     * @param master_file Upload data from this file
     * @param local_chunks
     */
    void (*init)(upload_manager_t* self, int sock, int max, FILE* master_file,
                 chunk_list* local_chunks);

    /**
     * Finalize a upload_manager_t
     */
    void (*finalize)(upload_manager_t* self);

    /**
     * Create a new upload dataflow to upload chunk with given hash to given
     * address
     *
     * @param self Invoker
     * @param addr Destination address
     * @param hash The chunk hash
     * @return A new upload_t if success, NULL on error
     */
    upload_t* (*new_upload)(upload_manager_t* self, sockaddr_in_t* addr,
                            char* hash);

    /**
     * Retrive a upload_t by destination address
     *
     * @return A upload_t matches the destination address. NULL if not found
     */
    upload_t* (*get_upload_by_addr)(upload_manager_t* self,
                                    sockaddr_in_t* addr);

    /**
     * Get chunk by hash and load it into buf
     *
     * @param  self
     * @param  hash
     * @param  buf
     * @return      0 on success, -1 on error
     */
    int (*get_chunk)(upload_manager_t* self, char* hash, char* buf);

    /**
     * Handle GET
     *
     * @param self
     * @param from The source address of the the packet
     * @param p
     * @return 0 on success, -1 on error
     */
    int (*handle_GET)(upload_manager_t* self, sockaddr_in_t* from, packet* p);

    /**
     * handle ACK
     *
     * @param self
     * @param from The source address of the the packet
     * @param p
     * @return 0 on success, -1 on error
     */
    int (*handle_ACK)(upload_manager_t* self, sockaddr_in_t* from, packet* p);

    /**
     * handle timeout
     */
    void (*handle_timeout)(upload_manager_t* self);
};

upload_manager_t* upload_manager_create();

/**
 * Method in upload_manager_t
 */
void upload_manager_init(upload_manager_t*, int, int, FILE*, chunk_list*);
void upload_manager_finalize(upload_manager_t*);
upload_t* upload_manager_new_upload(upload_manager_t*, sockaddr_in_t*, char*);
upload_t* upload_manager_get_upload_by_addr(upload_manager_t* self,
        sockaddr_in_t* addr);
int upload_manager_get_chunk(upload_manager_t*, char*, char*);
int upload_manager_handle_GET(upload_manager_t*, sockaddr_in_t*, packet*);
int upload_manager_handle_ACK(upload_manager_t*, sockaddr_in_t*, packet*);
void upload_manager_handle_timeout(upload_manager_t*);

#endif
