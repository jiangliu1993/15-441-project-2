#include <assert.h>
#include <netinet/in.h>
#include <stdlib.h>
#include "message.h"
#include "debug.h"
#include "upload.h"

upload_t* upload_create()
{
    upload_t* instance = malloc(sizeof(upload_t));

    instance->init = upload_init;
    instance->finalize = upload_finalize;
    instance->handle_ACK = upload_handle_ACK;
    instance->handle_timeout = upload_handle_timeout;
    instance->send_data = upload_send_data;

    return instance;
}

void upload_init(upload_t* self, connection_t* conn)
{
    self->conn = conn;
    self->bytes_sent = 0;
    self->bytes_acked = 0;
    self->buf_len = CHUNK_DATA_SIZE;
    self->timestamp = now();
    self->queue = create_chunk_list();
}

void upload_finalize(upload_t* self)
{
    if (self->conn != NULL) {
        self->conn->finalize(self->conn);
        self->conn = NULL;
    }
    free_chunk_list(self->queue);
    free(self);
}

void upload_handle_ACK(upload_t* self, packet* p)
{
    connection_t* c = self->conn;

    self->bytes_acked += c->recv_ack(c, p);
    self->send_data(self);
    self->timestamp = now();
}

int upload_handle_timeout(upload_t* self)
{
    int cnt_timeout = 0;

    cnt_timeout = self->conn->handle_timeout(self->conn);

    return 0;
}

void upload_send_data(upload_t* self)
{
    int n;

    n = self->conn->send(self->conn, self->buffer + self->bytes_sent,
                         self->buf_len - self->bytes_sent);
    self->bytes_sent += n;
}
/******************************************************************************/
/******************************************************************************/
/****************************** Upload Manager ********************************/
upload_manager_t* upload_manager_create()
{
    upload_manager_t* instance = malloc(sizeof(upload_manager_t));

    instance->init = upload_manager_init;
    instance->finalize = upload_manager_finalize;
    instance->new_upload = upload_manager_new_upload;
    instance->get_upload_by_addr = upload_manager_get_upload_by_addr;
    instance->get_chunk = upload_manager_get_chunk;
    instance->handle_GET = upload_manager_handle_GET;
    instance->handle_ACK = upload_manager_handle_ACK;
    instance->handle_timeout = upload_manager_handle_timeout;

    return instance;
}

void upload_manager_init(upload_manager_t* self, int sock, int max,
                         FILE* master_file, chunk_list* local_chunks)
{
    int i;

    self->sock = sock;
    self->max = max;
    self->cnt = 0;
    self->upload_id = 1;
    self->local_chunks = local_chunks;
    self->master_file = master_file;
    self->upload_list = malloc(max * sizeof(upload_t*));
    for (i = 0; i < max; ++i)
        self->upload_list[i] = NULL;

}

void upload_manager_finalize(upload_manager_t* self)
{
    int i;
    upload_t* u;

    for (i = 0; i < self->max; ++i) {
        u = self->upload_list[i];
        if (u != NULL)
            u->finalize(u);
    }

    free(self);
}

upload_t* upload_manager_new_upload(upload_manager_t* self,
                                    sockaddr_in_t* addr, char* hash)
{
    int i;
    upload_t* u;
    connection_t* c;

    if (self->cnt >= self->max)
        return NULL;

    for (i = 0; i < self->max; ++i) {
        u = self->upload_list[i];
        if (u == NULL) {
            // Setup a new connection for the upload
            c = connection_create();
            c->init(c, self->upload_id++, C_SENDER, self->sock, addr);

            // Setup upload
            u = upload_create();
            u->init(u, c);
            // Fill upload's sender buffer with chunk data
            if (self->get_chunk(self, hash, u->buffer) == -1) {
                c->finalize(c);
                return NULL;
            }

            // Add the new upload to list in manager
            self->upload_list[i] = u;
            self->cnt++;
            return u;
        }
    }

    // Control should never reach here!
    assert(0);
    return NULL;
}

upload_t* upload_manager_get_upload_by_addr(upload_manager_t* self,
        sockaddr_in_t* addr)
{
    int i;
    upload_t* u;

    for (i = 0; i < self->max; ++i) {
        u = self->upload_list[i];
        if (u != NULL && u->conn->addr->sin_port == addr->sin_port &&
                u->conn->addr->sin_addr.s_addr == addr->sin_addr.s_addr)
            return u;
    }

    return NULL;
}

int upload_manager_get_chunk(upload_manager_t* self, char* hash, char* buf)
{
    chunk* c = chunk_list_retrieve(self->local_chunks, hash);

    if (c == NULL) return -1;

    // Seek to start position of the chunk
    if (fseek(self->master_file, c->id * CHUNK_DATA_SIZE, SEEK_SET) != 0) {
        DEBUG_PERROR("upload_manager_get_chunk: fseek error");
        return -1;
    }

    // Read data into buffer
    if (fread(buf, 1, CHUNK_DATA_SIZE, self->master_file) != CHUNK_DATA_SIZE) {
        DEBUG_PERROR("upload_manager_get_chunk: fread error");
        return -1;
    }

    return 0;
}

int upload_manager_handle_GET(upload_manager_t* self, sockaddr_in_t* from,
                              packet* p)
{
    assert(p != NULL);

    upload_t* u;

    if (p->payload->data_len != CHUNK_HASH_LEN) {
        DPRINTF(DEBUG_ERRS, "upload_manager_handle_GET: Invalid GET packet\n");
        return -1;
    }

    u = self->get_upload_by_addr(self, from);
    if (u == NULL) {
        u = self->new_upload(self, from, p->payload->data);
        if (!u)
            return -1;
    } else {
        chunk_list_add(u->queue, create_chunk(-1, p->payload->data));
    }

    u->send_data(u);

    return 0;
}

int upload_manager_handle_ACK(upload_manager_t* self, sockaddr_in_t* from,
                              packet* p)
{
    upload_t* u;
    chunk* c;
    int i;

    u = self->get_upload_by_addr(self, from);
    if (u == NULL) return -1;

    u->handle_ACK(u, p);

    // All bytes in the chunk have been ACKed, finalize the upload
    if (u->bytes_acked == CHUNK_DATA_SIZE) {
        while (u->queue->length) {
            c = u->queue->head;

            i = self->get_chunk(self, c->hash, u->buffer);

            chunk_list_remove(u->queue, c->id);
            if (i == -1) continue;

            u->bytes_sent = 0;
            u->bytes_acked = 0;
            u->timestamp = now();

            u->send_data(u);

            return 0;
        }
        for (i = 0; i < self->max; ++i)
            if (self->upload_list[i] == u) {
                self->upload_list[i] = NULL;
                u->finalize(u);
                self->cnt--;
            }
    }

    return 0;
}

void upload_manager_handle_timeout(upload_manager_t* self)
{
    int i;
    upload_t* u;

    for (i = 0; i < self->max; ++i) {
        u = self->upload_list[i];
        if (u != NULL) {
            u->handle_timeout(u);
        }
    }
}
