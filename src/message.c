#include <stdlib.h>
#include <string.h>
#include "message.h"

int send_whohas_to(int sock, sockaddr_in_t *addr, chunk_list *list)
{
    return send_chunk_list_to(sock, addr, list, T_WHOHAS);
}

int send_ihave_to(int sock, sockaddr_in_t *addr, chunk_list *list)
{
    return send_chunk_list_to(sock, addr, list, T_IHAVE);
}

int send_chunk_list_to(int sock, sockaddr_in_t *addr, chunk_list *list,
                       unsigned char type)
{
    packet *p;
    chunk *c;
    char payload_buf[MAX_SIZE - HEADER_SIZE];
    int payload_len = 0;

    c = list->head;
    while (c) {
        if (payload_len == 0) {
            payload_buf[0] = 0;
            payload_buf[1] = 0;
            payload_buf[2] = 0;
            payload_buf[3] = 0;
            payload_len = 4;
        }
        if (payload_len + CHUNK_HASH_LEN > MAX_SIZE - HEADER_SIZE) {
            p = create_packet(type, 0, 0, payload_buf, payload_len);
            if (send_packet_to(sock, addr, p) == 0) {
                free_packet(p);
                return 0;
            }
            free_packet(p);
            payload_len = 0;
            continue;
        }
        memcpy(payload_buf + payload_len, c->hash, CHUNK_HASH_LEN);
        payload_len += CHUNK_HASH_LEN;
        payload_buf[0]++;
        c = c->next;
    }
    if (payload_len > 0) {
        p = create_packet(type, 0, 0, payload_buf, payload_len);
        if (send_packet_to(sock, addr, p) == 0) {
            free_packet(p);
            return 0;
        }
        free_packet(p);
    }
    return 1;
}

chunk_list *parse_whohas(packet *p)
{
    if (p->header->type != T_WHOHAS)
        return NULL;
    return parse_chunk_list(p);
}

chunk_list *parse_ihave(packet *p)
{
    if (p->header->type != T_IHAVE)
        return NULL;
    return parse_chunk_list(p);
}

chunk_list *parse_chunk_list(packet *p)
{
    return bytes_to_chunk_list(p->payload->data, p->payload->data_len);
}
