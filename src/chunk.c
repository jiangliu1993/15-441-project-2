#include <assert.h>
#include <stdlib.h>
#include <string.h>
#include "chunk.h"

chunk *create_chunk(int id, char *hash)
{
    chunk *c;

    c = (chunk *)malloc(sizeof(chunk));
    if (c == NULL)
        return NULL;
    c->id = id;
    memcpy(c->hash, hash, CHUNK_HASH_LEN);
    c->next = NULL;

    return c;
}

chunk *chunk_clone(chunk *c)
{
    return create_chunk(c->id, c->hash);
}

chunk *bytes_to_chunk(char *buf, int buf_len)
{
    if (buf_len < CHUNK_HASH_LEN)
        return NULL;
    return create_chunk(-1, buf);
}

void free_chunk(chunk *c)
{
    free(c);
}

chunk_list *create_chunk_list()
{
    chunk_list *list;

    list = (chunk_list *)malloc(sizeof(chunk_list));
    if (list == NULL)
        return NULL;
    list->length = 0;
    list->head = NULL;
    list->tail = NULL;

    return list;
}

chunk_list* clone_chunk_list(chunk_list *list) {
    chunk_list* clone = create_chunk_list();
    chunk* c;

    for (c = list->head; c != NULL; c = c->next)
        chunk_list_add(clone, chunk_clone(c));

    return clone;
}

chunk_list *bytes_to_chunk_list(char *buf, int buf_len)
{
    chunk_list *list;
    chunk *c;
    int num, i;

    if (buf_len < 4)
        return NULL;
    num = buf[0];
    if (num <= 0 || num * CHUNK_HASH_LEN + 4 > buf_len)
        return NULL;
    list = create_chunk_list();
    if (list == NULL)
        return NULL;
    for (i = 0; i < num; i++) {
        c = bytes_to_chunk(buf + 4 + i * CHUNK_HASH_LEN, CHUNK_HASH_LEN);
        if (c == NULL) {
            free_chunk_list(list);
            return NULL;
        }
        chunk_list_add(list, c);
    }

    return list;
}

void chunk_list_add(chunk_list *list, chunk *c)
{
    if (list->head == NULL) {
        list->head = c;
        list->tail =c;
    } else {
        list->tail->next = c;
        list->tail = c;
    }
    list->length++;
}

void chunk_list_remove(chunk_list *list, int id)
{
    chunk *c;
    chunk *temp;

    temp = NULL;
    c = list->head;
    while (c) {
        if (c->id == id) {
            if (c == list->head) {
                list->head = c->next;
                if (c == list->tail)
                    list->tail = NULL;
            } else if (c == list->tail) {
                list->tail = temp;
                temp->next = NULL;
            } else {
                temp->next = c->next;
            }
            free_chunk(c);
            list->length--;
            return;
        }
        temp = c;
        c = c->next;
    }
}

void free_chunk_list(chunk_list *list)
{
    chunk *c;
    chunk *temp;

    c = list->head;
    while(c) {
        temp = c->next;
        free_chunk(c);
        c = temp;
    }
    free(list);
}

chunk_list* chunk_list_from_fp(FILE *fp)
{
    char buf[CHUNK_BUFSIZE];
    int chunk_id, tmp, i;
    chunk *pc;
    chunk_list *chunks = create_chunk_list();

    while ((tmp = fscanf(fp, "%d %40s", &chunk_id, buf)) != EOF) {
        if (tmp < 2) {
            free_chunk_list(chunks);
            perror("chunk_list_from_fp fscanf()");
            return NULL;
        }
        for (i = 0; i < 20; ++ i) {
            sscanf(buf + i * 2, "%2x", &tmp);
            buf[i] = (char)tmp;
        }
        pc = create_chunk(chunk_id, buf);
        chunk_list_add(chunks, pc);
    }

    return chunks;
}

chunk_list* chunk_list_from_file(char *fname)
{
    FILE *fp;

    if ((fp = fopen(fname, "r")) == NULL) {
        perror("chunk_list_from_file fopen()");
        return NULL;
    }

    return chunk_list_from_fp(fp);
}

chunk* chunk_list_retrieve(chunk_list *list, char *hash)
{
    assert(list != NULL);

    chunk *c;

    for (c = list->head; c != NULL; c = c->next)
        if (hash_equal(hash, c->hash))
            return c;

    return NULL;
}

int hash_equal(char* h1, char* h2)
{
    int i;

    for (i = 0; i < CHUNK_HASH_LEN; ++i)
        if (h1[i] != h2[i])
            return 0;

    return 1;
}
