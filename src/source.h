#ifndef __SOURCE_H__
#define __SOURCE_H__

#include "chunk.h"
#include "packet.h"

/**
 * Source
 *
 * Indicate a potential download source.
 */
typedef struct source_s source_t;
struct source_s {
    sockaddr_in_t* addr;
    /**
     * Chunks that are specified in the IHAVE message from the source
     */
    chunk_list* chunks;
    int busy; //<! Indicate if there is an ongoing download from this source
    source_t* next;

    /**
     * Init a source
     */
    void (*init)(source_t* self, sockaddr_in_t* addr, chunk_list* chunks);

    /**
     * Finalize a source
     */
    void (*finalize)(source_t* self);

    /**
     * Check if the source contains a chunk with given hash
     *
     * @return 1 if the source contains a chunk with given hash, otherwise 0
     */
    int (*contains)(source_t* self, char* hash);
};

source_t* source_create();

void source_init(source_t*, sockaddr_in_t*, chunk_list*);
void source_finalize(source_t*);
int source_contains(source_t*, char*);

/**
 * Source Manager
 *
 * Manage download sources. Provide methods to add and remove a download source
 * as well as searching for an idle source which provide chunks with a specific
 * hash.
 */
typedef struct source_manager_s source_manager_t;
struct source_manager_s {
    source_t* source_list;

    /**
     * Init a source manager
     */
    void (*init)(source_manager_t* self);

    /**
     * Finalize a source manager
     */
    void (*finalize)(source_manager_t* self);

    /**
     * Create a new source identified by addr with chunks
     *
     * @param addr The address of the new source
     * @param chunks Chunks declared in the IHAVE message from this source
     */
    void (*add)(source_manager_t* self, sockaddr_in_t* addr, chunk_list* chunks);

    /**
     * Remove a source identified by addr
     *
     * @param The address of the to-be-removed source
     */
    void (*remove)(source_manager_t* self, sockaddr_in_t* addr);

    /**
     * Retrieve an idle source which contains chunk with given hash
     */
    source_t* (*query)(source_manager_t* self, char* hash);
};

source_manager_t* source_manager_create();

void source_manager_init(source_manager_t*);
void source_manager_finalize(source_manager_t*);
void source_manager_add(source_manager_t*, sockaddr_in_t*, chunk_list*);
void source_manager_remove(source_manager_t*, sockaddr_in_t*);
source_t* source_manager_query(source_manager_t*, char*);

#endif