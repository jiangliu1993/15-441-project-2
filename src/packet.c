#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include "packet.h"
#include "spiffy.h"

packet *create_packet(unsigned char type, unsigned int sequence,
                      unsigned int acknowledge, char *data,
                      unsigned short data_len)
{
    packet *p;
    packet_header *header;
    packet_payload *payload;

    p = (packet *)malloc(sizeof(packet));
    if (p == NULL)
        return NULL;
    header = create_header(type, sequence, acknowledge, HEADER_SIZE,
                           HEADER_SIZE + data_len);
    if (header == NULL) {
        free(p);
        return NULL;
    }
    payload = create_payload(data, data_len);
    if (payload == NULL) {
        free(p);
        free(header);
        return NULL;
    }
    p->header = header;
    p->extend_header = NULL;
    p->payload = payload;

    return p;
}

packet* clone_packet(packet* p)
{
    return create_packet(p->header->type, p->header->sequence,
                         p->header->acknowledge, p->payload->data,
                         p->payload->data_len);
}

packet *create_extend_packet(unsigned char type, unsigned int sequence,
                             unsigned int acknowledge, char *data,
                             unsigned short data_len)
{
    packet *p;
    packet_header *header;
    packet_extend_header *extend_header;
    packet_payload *payload;

    p = (packet *)malloc(sizeof(packet));
    if (p == NULL)
        return NULL;
    header = create_header(type, sequence, acknowledge,
                           HEADER_SIZE + EXTEND_SIZE,
                           HEADER_SIZE + EXTEND_SIZE + data_len);
    if (header == NULL) {
        free(p);
        return NULL;
    }
    extend_header = create_extend_header();
    if (extend_header == NULL) {
        free(p);
        free(header);
        return NULL;
    }
    payload = create_payload(data, data_len);
    if (payload == NULL) {
        free(p);
        free(header);
        free(extend_header);
        return NULL;
    }
    p->header = header;
    p->extend_header = extend_header;
    p->payload = payload;

    return p;
}

int send_packet_to(int sock, sockaddr_in_t *addr, packet *p)
{
    socklen_t addr_len;
    char buf[MAX_SIZE];

    if (packet_to_bytes(p, buf, MAX_SIZE) == 0)
        return 0;

    addr_len = sizeof(sockaddr_in_t);

    if (spiffy_sendto(sock, buf, p->header->total_len, 0,
                      (struct sockaddr *)addr, addr_len) == -1) {
        close(sock);
        return 0;
    }

    return 1;
}

int packet_to_bytes(packet *p, char *buf, int buf_len)
{
    if (p->header->total_len > buf_len)
        return 0;
    if (header_to_bytes(p->header, buf, HEADER_SIZE) == 0)
        return 0;
    if (p->extend_header) {
        if (extend_header_to_bytes(p->extend_header,
                                   buf + HEADER_SIZE, EXTEND_SIZE) == 0)
            return 0;
    }

    if (p->payload->data_len > 0 && payload_to_bytes(p->payload, buf +
            p->header->header_len, p->payload->data_len) == 0)
        return 0;

    return p->header->total_len;
}

packet *bytes_to_packet(char *buf, int buf_len)
{
    packet *p = NULL;
    packet_header *header = NULL;
    packet_extend_header *extend_header = NULL;
    packet_payload *payload = NULL;

    if (buf_len < HEADER_SIZE) {
        fprintf(stderr, "bad buf_len: %d\n", buf_len);
        return NULL;
    }
    p = (packet *)malloc(sizeof(packet));
    if (p == NULL) {
        perror("malloc fail");
        return NULL;
    }

    header = bytes_to_header(buf, HEADER_SIZE);
    if (header == NULL) {
        fprintf(stderr, "bad header\n");
        free(p);
        return NULL;
    }
    if (header->magic != MAGIC_NUM ||
            header->version != VERSION_NUM ||
            header->type > MAX_TYPE ||
            header->total_len != buf_len) {
        fprintf(stderr, "corrupted packet %d %d %d %d\n", header->magic,
                header->version, header->type, header->total_len);
        free(p);
        free(header);
        return NULL;
    }
    if (header->header_len != HEADER_SIZE) {
        if (header->header_len != HEADER_SIZE + EXTEND_SIZE) {
            fprintf(stderr, "bad header_len\n");
            free(p);
            free(header);
            return NULL;
        } else {
            extend_header = bytes_to_extend_header(buf + HEADER_SIZE,
                                                   EXTEND_SIZE);
            if (extend_header == NULL) {
                fprintf(stderr, "bad extend_header\n");
                free(p);
                free(header);
                return NULL;
            }
            if (extend_header->group != GROUP_NUM) {
                fprintf(stderr, "bad group\n");
                free(p);
                free(header);
                free(extend_header);
                return NULL;
            }
        }
    }
    payload = bytes_to_payload(buf + header->header_len,
                               buf_len - header->header_len);
    if (payload == NULL) {
        fprintf(stderr, "bad payload\n");
        free(p);
        free(header);
        free(extend_header);
        return NULL;
    }
    p->header = header;
    p->extend_header = extend_header;
    p->payload = payload;

    return p;
}

void free_packet(packet *p)
{
    if (p->header != NULL)
        free_header(p->header);
    if (p->extend_header != NULL) free_extend_header(p->extend_header);
    if (p->payload != NULL)
        free_payload(p->payload);
    free(p);
}

packet_header *create_header(unsigned char type, unsigned int sequence,
                             unsigned int acknowledge,
                             unsigned short header_len,
                             unsigned short total_len)
{
    packet_header *header;

    header = (packet_header *)malloc(sizeof(packet_header));
    if (header == NULL)
        return NULL;
    header->magic = MAGIC_NUM;
    header->version = VERSION_NUM;
    header->type = type;
    header->header_len = header_len;
    header->total_len = total_len;
    header->sequence = sequence;
    header->acknowledge = acknowledge;

    return header;
}

int header_to_bytes(packet_header *header, char *buf, int buf_len)
{
    if (HEADER_SIZE > buf_len)
        return 0;
    ushort_to_bytes(header->magic, buf);
    uchar_to_bytes(header->version, buf + 2);
    uchar_to_bytes(header->type, buf + 3);
    ushort_to_bytes(header->header_len, buf + 4);
    ushort_to_bytes(header->total_len, buf + 6);
    uint_to_bytes(header->sequence, buf + 8);
    uint_to_bytes(header->acknowledge, buf + 12);

    return HEADER_SIZE;
}

packet_header *bytes_to_header(char *buf, int buf_len)
{
    packet_header *header;

    if (buf_len < HEADER_SIZE)
        return NULL;
    header = (packet_header *)malloc(sizeof(packet_header));
    if (header == NULL)
        return NULL;
    header->magic = bytes_to_ushort(buf);
    header->version = bytes_to_uchar(buf + 2);
    header->type = bytes_to_uchar(buf + 3);
    header->header_len = bytes_to_ushort(buf + 4);
    header->total_len = bytes_to_ushort(buf + 6);
    header->sequence = bytes_to_uint(buf + 8);
    header->acknowledge = bytes_to_uint(buf + 12);

    return header;
}

void free_header(packet_header *header)
{
    free(header);
}

packet_extend_header *create_extend_header()
{
    packet_extend_header *extend_header;

    extend_header = (packet_extend_header *)malloc(
                        sizeof(packet_extend_header));
    if (extend_header == NULL)
        return NULL;
    extend_header->group = GROUP_NUM;

    return extend_header;
}

int extend_header_to_bytes(packet_extend_header *extend_header,
                           char *buf, int buf_len)
{
    if (EXTEND_SIZE > buf_len)
        return 0;
    ushort_to_bytes(extend_header->group, buf);
    /* Padding to maintain alignment */
    ushort_to_bytes(0, buf + 2);

    return EXTEND_SIZE;
}

packet_extend_header *bytes_to_extend_header(char *buf, int buf_len)
{
    packet_extend_header *extend_header;

    if (buf_len < EXTEND_SIZE)
        return NULL;
    extend_header = (packet_extend_header *)malloc(
                        sizeof(packet_extend_header));
    if (extend_header == NULL)
        return NULL;
    extend_header->group = bytes_to_ushort(buf);

    return extend_header;
}

void free_extend_header(packet_extend_header *extend_header)
{
    free(extend_header);
}

packet_payload *create_payload(char *data, unsigned short data_len)
{
    packet_payload *payload;

    payload = (packet_payload *)malloc(sizeof(packet_payload));
    if (payload == NULL)
        return NULL;
    if (data_len == 0) {
        payload->data = NULL;
        payload->data_len = 0;
        return payload;
    }
    payload->data = (char *)malloc(data_len);
    if (payload->data == NULL) {
        free(payload);
        return NULL;
    }
    memcpy(payload->data, data, data_len);
    payload->data_len = data_len;

    return payload;
}

int payload_to_bytes(packet_payload *payload, char *buf, int buf_len)
{
    if (payload->data_len > buf_len)
        return 0;
    memcpy(buf, payload->data, payload->data_len);

    return payload->data_len;
}

packet_payload *bytes_to_payload(char *buf, int buf_len)
{
    if (buf_len < 0)
        return NULL;
    return create_payload(buf, buf_len);
}

void free_payload(packet_payload *payload)
{
    if (payload->data != NULL)
        free(payload->data);
    free(payload);
}

void uint_to_bytes(unsigned int value, char *buf)
{
    buf[0] = (value >> 24) & 0xFF;
    buf[1] = (value >> 16) & 0xFF;
    buf[2] = (value >> 8) & 0xFF;
    buf[3] = value & 0xFF;
}

void ushort_to_bytes(unsigned short value, char *buf)
{
    buf[0] = (value >> 8) & 0xFF;
    buf[1] = value & 0xFF;
}

void uchar_to_bytes(unsigned char value, char *buf)
{
    buf[0] = value & 0xFF;
}

unsigned int bytes_to_uint(char *buf)
{
    unsigned int value = 0;

    value |= (unsigned char)buf[0];
    value <<= 8;
    value |= (unsigned char)buf[1];
    value <<= 8;
    value |= (unsigned char)buf[2];
    value <<= 8;
    value |= (unsigned char)buf[3];

    return value;
}

unsigned short bytes_to_ushort(char *buf)
{
    unsigned short value = 0;

    value |= (unsigned char)buf[0];
    value <<= 8;
    value |= (unsigned char)buf[1];

    return value;
}

unsigned char bytes_to_uchar(char *buf)
{
    unsigned char value = 0;

    value |= (unsigned char)buf[0];

    return value;
}
