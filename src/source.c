#include <stdlib.h>
#include <netinet/in.h>
#include "source.h"

static int addr_equal(sockaddr_in_t* a1, sockaddr_in_t* a2) {
    return (a1->sin_port == a2->sin_port && a1->sin_addr.s_addr ==
            a2->sin_addr.s_addr);
}

source_t* source_create() {
    source_t* instance = malloc(sizeof(source_t));

    instance->init = source_init;
    instance->finalize = source_finalize;
    instance->contains = source_contains;

    return instance;
}

void source_init(source_t* self, sockaddr_in_t* addr, chunk_list* chunks) {
    self->addr = addr;
    self->chunks = clone_chunk_list(chunks);
    self->next = NULL;
    self->busy = 0;
}

void source_finalize(source_t* self) {
    free_chunk_list(self->chunks);
    free(self);
}

int source_contains(source_t* self, char* hash) {
    return (chunk_list_retrieve(self->chunks, hash) != NULL);
}

source_manager_t* source_manager_create() {
    source_manager_t* instance = malloc(sizeof(source_manager_t));

    instance->init = source_manager_init;
    instance->finalize = source_manager_finalize;
    instance->add = source_manager_add;
    instance->remove = source_manager_remove;
    instance->query = source_manager_query;

    return instance;
}

void source_manager_init(source_manager_t* self) {
    self->source_list = NULL;
}

void source_manager_finalize(source_manager_t* self) {
    source_t* s;

    for (s = self->source_list; s != NULL; s = s->next)
        s->finalize(s);

    free(self);
}

void source_manager_add(source_manager_t* self, sockaddr_in_t* addr,
        chunk_list* chunks) {
    source_t *s, *prev;

    for (s = self->source_list; s != NULL; s = s->next) {
        if (addr_equal(s->addr, addr)) {
            free_chunk_list(s->chunks);
            s->chunks = clone_chunk_list(chunks);
            return;
        }
        prev = s;
    }

    s = source_create();
    s->init(s, addr, chunks);
    if (self->source_list == NULL)
        self->source_list = s;
    else
        prev->next = s;
}

void source_manager_remove(source_manager_t* self, sockaddr_in_t* addr) {
    source_t *s, *prev = NULL;

    for (s = self->source_list; s != NULL; s = s->next) {
        if (addr_equal(s->addr, addr)) {
            if (prev == NULL)
                self->source_list = s->next;
            else
                prev->next = s->next;

            s->finalize(s);
            return;
        }
        prev = s;
    }
}

source_t* source_manager_query(source_manager_t* self, char* hash) {
    source_t* s;

    for (s = self->source_list; s != NULL; s = s->next)
        if (!s->busy && s->contains(s, hash))
            return s;

    return NULL;
}
