#ifndef __CONSTANTS_H__
#define __CONSTANTS_H__

/*********************************** Chunk ************************************/
#define CHUNK_DATA_SIZE 512 * 1024
#define CHUNK_HASH_LEN 20
/* Maximum number of chunks in a message */
#define PEER_CHUNK_PER_MSG 15

/*********************************** Packet ***********************************/
#define MAX_SIZE 1500
#define HEADER_SIZE 16
#define EXTEND_SIZE 4
#define MAX_PAYLOAD_SIZE 1400

/*************************** Timeout(unit: 10^-6s) ****************************/
/**
 * If ACK is not received within this among of time after sending a packet, the
 * packet will be considered as timeout.
 */
#define PACKET_DEFAULT_TIMEOUT 2500000
#define SELECT_TIMEOUT         1000000
/**
 * After starting downloading a chunk, if no DATA packet is received within this
 * among of time, the chunk will be timeout.
 */
#define DOWNLOAD_CHUNK_TIMEOUT 5000000
#define UPLOAD_CHUNK_TIMEOUT   8000000


#endif
